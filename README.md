Basic Hash Map Implementation in C++
------------------------------------
This is simple and fast Hash code in C++.Easy to implement.This is the idea of perfect hashing - to use hash table of second level for elements that have the same hash value.Using this class, solve the following problem. The input has a set of different numbers followed by a set of requests, each of which is represented by an integer number.

Sherrell Covarrubias - Programmer at http://www.washingtontractor.com/

![cplusmaphash006.png](https://bitbucket.org/repo/j47AAa/images/3670694978-cplusmaphash006.png)










Compile and run tests

	make

### Example Usage

Define a hash function by overloading operator() for integer type key

	struct MyKeyHash {
    	unsigned long operator()(const int& k) const
    	{	
        	return k % 10;
    	}
	};
	
Declare a hashmap with integer type key and string type value pair

	HashMap<int, string, MyKeyHash> hmap;

Put a key-value pair into the hashmap

	hmap.put(1, "1");

Get the value by key; returns true if successful with value updated

	string value;
	bool result = hmap.get(2, value);